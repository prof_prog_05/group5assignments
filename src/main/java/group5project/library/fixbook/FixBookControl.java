package library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class FixBookControl {
	
	private FixBookUI ui; //Fixed object name to ui and class name to FixBookUI --Athulya
	private enum ControlState {INITIALISED, READY, FIXING};//Fixed brackets and enum name --Athulya
	private ControlState state; // Fixed variable name to state ---Athulya
	private Library library;//fixed object name to library and space---Athulya
	private Book currentBook; //Fixed object name to currentBook ---Athulya


	public FixBookControl() {//Fixed constuctor name to FixBookControl ---Athulya
		this.library = Library.getInstance(); //Fixed method name to getInstance --Athulya 
		state = ControlState.INITIALISED;
	}
	
	
	public void setUi(FixBookUI ui) {//Fixed setUi method name and class name FixBookUI--Athulya
		if (!state.equals(ControlState.INITIALISED)){ //Fixed brackets--Athulya
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
		}	
		this.ui = ui; //Fixed variable name to ui --athulya
		ui.setState(FixBookUI.UiState.READY); //Fixed method name to setState and Class name FixBookUI and Enum name  --Athulya
		state = ControlState.READY;		
	}


	public void scanBook(int bookId) { //Fixed method name scanBook and variable name bookId---Athulya
		if (!state.equals(ControlState.READY)) {// Fixed brackets --Athulya
			throw new RuntimeException("FixBookControl: cannot call scanBook except in READY state");
		}	
		currentBook = library.toGetBook(bookId); //Fixed the method name toGetBook --Athulya
		
		if (currentBook == null) {
			ui.display("Invalid bookId"); //Fixed object name to ui and method name display---Athulya
			return;
		}
		if (!currentBook.isDamaged()) { //Fixed method name isDamaged --Athulya
			ui.display("Book has not been damaged"); //Fixed object name to ui--Athulya
			return;
		}
		ui.display(currentBook.toString());//Fixed object name to ui---AThulya
		ui.setState(FixBookUI.UiState.FIXING);
		state = ControlState.FIXING;		
	}


	public void fixBook(boolean mustFix) { //Fixed method name to fixBook and variable name mustFix---Athulya
		if (!state.equals(ControlState.FIXING)) {//Bracket Fixed --Athulya
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
		}	
		if (mustFix){ //Bracket fixed --Athulya
			library.toRepairBook(currentBook);//Fixed methodname to toRepairBook --Athulya
		}
		currentBook = null;
		ui.setState(FixBookUI.UiState.READY);//Fixed object name to ui--Athulya
		state = ControlState.READY;		
	}

	
	public void completeScan() { //Fixed method name completeScan-- Athulya
		if (!state.equals(ControlState.READY)) {//Bracket Fixed--Athulya
			throw new RuntimeException("FixBookControl: cannot call completeScan except in READY state");
		}	
		ui.setState(FixBookUI.UiState.COMPLETED);	//Fixed object namer to ui--Athulya	
	}

}
