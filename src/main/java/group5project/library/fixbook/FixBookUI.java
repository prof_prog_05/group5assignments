package library.fixbook;
import java.util.Scanner;


public class FixBookUI {// Fixed class name to FixBookUI --Athulya

	public static enum UiState {INITIALISED, READY, FIXING, COMPLETED};// Changed enum name to UiState and removed space from bracket--Athulya

	private FixBookControl control; //Fixed to object name control and class name to FixBookControl---Athulya
	private Scanner input;//Fixed input variable name--Athulya
	private UiState state; //Fixed variable name state--Athulya 

	
	public FixBookUI(FixBookControl control) { // Fixed constructor name FixBookUI--Athulya
		this.control = control;
		input = new Scanner(System.in);
		state = UiState.INITIALISED; //Fixed state variable name--Athulya
		control.setUi(this); //Fixed method name setUi --Athulya
	}


	public void setState(UiState state) {// Fixed method name setState--Athulya
		this.state = state;
	}

	
	public void run() {//Fixed method name run--Athulya
		output("Fix Book Use Case UI\n");//Fixed method name output--Athulya
		
		while (true) {
			
			switch (state) {
			
			case READY:
				String bookEntryString = input("Scan Book (<enter> completes): ");//Fixed bookEntryString variable --Athulya
				if (bookEntryString.length() == 0) { //Fixed bracket---Athulya
					control.completeScan();//Fixed method name completeScan--Athulya
				}
				else {
					try {
						int bookId = Integer.parseInt(bookEntryString);// Fixed variable name bookId and unnecessary unboxing to Integer.parseInt--Athulya
						control.scanBook(bookId);
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");
					}
				}
				break;	
				
			case FIXING:
				String ans = input("Fix Book? (Y/N) : "); //Fixed variable name to ans---Athulya
				boolean fix = false;//Fixed variable name to fix---Athulya
				if (ans.toUpperCase().equals("Y")) { //Fixed bracket--Athulya
					fix = true;
				}
				control.fixBook(fix); //Fixed method name fixBook---Athulya
				break;
								
			case COMPLETED:
				output("Fixing process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + state);//Fixed class name FixBookUI--Athulya			
			
			}		
		}
		
	}

	
	private String input(String prompt) { //Fixed method name input --Athulya
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}
	

	public void display(Object object) {//Fixed method name display--Athulya
		output(object);//Fixed method name output--Athulya
	}
	
	
}
