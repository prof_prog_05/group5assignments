package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

	private ReturnBookUI ui;    //fix Ui --> ui --ketulkumar
	private enum ControlState { INITIALISED, READY, INSPECTING };  //fix enum cOnTrOl_sTaTe --> ControlState --ketulkumar
	private ControlState state;                                    //fix sTaTe --> state --ketulkumar
	
	private Library library;        //fix lIbRaRy --> library --ketulkumar
	private Loan currentLoan;      //fix CurrENT_loan --> currentLoan --ketulkumar
	

	public ReturnBookControl() {
		this.library = Library.getInstance();   //fix GeTiNsTaNcE --> getInstance --ketulkumar
		state = ControlState.INITIALISED;
	}
	
	
	public void setUi(ReturnBookUI ui) {       //fix method sEt_uI --> setUi, parameter uI --> ui  --ketulkumar
		if (!state.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
		
		this.ui = ui;
		ui.setState(ReturnBookUI.UiState.READY);      //fix method sEt_sTaTe --> setState, enum uI_sTaTe --> UiState  --Ketulkumar 
		state = ControlState.READY;		
	}


	public void scanBook(int bookId) {     //fix method bOoK_sCaNnEd --> scanBook, parameter bOoK_iD --> bookId --ketulkumar
		if (!state.equals(ControlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
		
		Book currentBook = library.toGetBook(bookId);   //fix Book object cUrReNt_bOoK --> currentBook --ketulkumar
		
		if (currentBook == null) {
			ui.display("Invalid Book Id");          //fix method DiSpLaY --> display  --Ketulkumar
			return;
		}
		if (!currentBook.isOnLoan()) {
			ui.display("Book has not been borrowed");
			return;
		}		
		currentLoan = library.toGetLoanByBookId(bookId);	
		double overDueFine = 0.0;         //fix variable Over_Due_Fine --> overDueFine  --ketulkumar
		if (currentLoan.isOverDue()) 
			overDueFine = library.toCalculateOverDueFine(currentLoan);
		
		ui.display("Inspecting");
		ui.display(currentBook.toString());
		ui.display(currentLoan.toString());
		
		if (currentLoan.isOverDue()) 
			ui.display(String.format("\nOverdue fine : $%.2f", overDueFine));
		
		ui.setState(ReturnBookUI.UiState.INSPECTING);
		state = ControlState.INSPECTING;		
	}


	public void completeScanning() {       //fix method sCaNnInG_cOmPlEtE --> completeScanning  --Ketulkumar
		if (!state.equals(ControlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call completeScanning except in READY state");
			
		ui.setState(ReturnBookUI.UiState.COMPLETED);		
	}


	public void dischargeLoan(boolean isDamaged) {        //fix method dIsChArGe_lOaN --> dischargeLoan, parameter iS_dAmAgEd --> isDamaged  --Ketulkumar
		if (!state.equals(ControlState.INSPECTING)) 
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
		
		library.toDischargeLoan(currentLoan, isDamaged);
		currentLoan = null;
		ui.setState(ReturnBookUI.UiState.READY);
		state = ControlState.READY;				
	}


}
