package library.returnBook;
import java.util.Scanner;


public class ReturnBookUI {

	public static enum UiState { INITIALISED, READY, INSPECTING, COMPLETED };  //fix uI_sTaTe --> UiState  --Ketulkumar//fix uI_sTaTe --> UiState  --Ketulkumar

	private ReturnBookControl control;      //Fix object CoNtRoL --> control  --Ketulkumar
	private Scanner input;                  //Fix object iNpUt --> input  --Ketulkumar
	private UiState state;                  //Fix StATe --> state  --Ketulkumar

	
	public ReturnBookUI(ReturnBookControl control) {        //Fix parameter object cOnTrOL --> control  --Ketulkumar
		this.control = control;
		input = new Scanner(System.in);
		state = UiState.INITIALISED;
		control.setUi(this);
	}


	public void run() {             //Fix method RuN --> run  --Ketulkumar
		output("Return Book Use Case UI\n");        //Fix oUtPuT --> output  --Ketulkumar
		
		while (true) {
			
			switch (state) {
			
			case INITIALISED:
				break;
				
			case READY:
				String bookInputString = input("Scan Book (<enter> completes): ");        //Fix variable BoOk_InPuT_StRiNg --> bookInputString, iNpUt-->input  --Ketulkumar
				if (bookInputString.length() == 0) 
					control.completeScanning();     //fix method sCaNnInG_cOmPlEtE --> completeScanning  --Ketulkumar
				
				else {
					try {
						int bookId = Integer.parseInt(bookInputString);      //Fix variable Book_Id --> bookId, Remove unnecessary unboxing  --Ketulkumar
						control.scanBook(bookId);       //fix method bOoK_sCaNnEd --> scanBook --ketulkumar
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");
					}					
				}
				break;				
				
				
			case INSPECTING:
				String ans = input("Is book damaged? (Y/N): ");     //Fix variable AnS --> ans  --Ketulkumar
				boolean isDamaged = false;                         //Fix variable Is_DAmAgEd --> isDamaged  --Ketulkumar
				if (ans.toUpperCase().equals("Y")) 					
					isDamaged = true;
				
				control.dischargeLoan(isDamaged);      //fix method dIsChArGe_lOaN --> dischargeLoan  --Ketulkumar
			
			case COMPLETED:
				output("Return processing complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("ReturnBookUI : unhandled state :" + state);			
			}
		}
	}

	
	private String input(String prompt) {       //Fix parameter PrOmPt --> prompt  --Ketulkumar
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {        //Fix object parameter ObJeCt --> object  --Ketulkumar
		System.out.println(object);
	}
	
			
	public void display(Object object) {    //fix method DiSpLaY --> display  --Ketulkumar
		output(object);
	}
	
	public void setState(UiState state) {      //fix method sEt_sTaTe --> setState  --Ketulkumar
		this.state = state;
	}

	
}
