package library;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import library.borrowbook.BorrowBookUi;
import library.borrowbook.BorrowBookControl;  //fix package import bORROW_bOOK_cONTROL-->BorrowBookControl --ketulkumar
import library.entities.Book;
import library.entities.Calendar;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;
import library.fixbook.FixBookUI;
import library.fixbook.FixBookControl;      //fix package name  --ketulkumar
import library.payfine.PayFineUI;
import library.payfine.PayFineControl;      //fix package name  --ketulkumar
import library.returnBook.ReturnBookUI;
import library.returnBook.ReturnBookControl;


public class Main {
	
	private static Scanner in;          //Fix IN --> in  --Ketulkumar
	private static Library library;     //Fix LIB --> library  --Ketulkumar
	private static String menu;         //Fix variable MENU --> menu  --Ketulkumar
	private static Calendar calendar;        //Fix CAL --> calendar  --Ketulkumar
	private static SimpleDateFormat simpleDateFormat;    //Fix SDF --> simpleDateFormat  --Ketulkumar
	
	
	private static String getMenu() {      //Fix Get_menu --> getMenu  --Ketulkumar
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nLibrary Main Menu\n\n")
		  .append("  M  : add member\n")
		  .append("  LM : list members\n")
		  .append("\n")
		  .append("  B  : add book\n")
		  .append("  LB : list books\n")
		  .append("  FB : fix books\n")
		  .append("\n")
		  .append("  L  : take out a loan\n")
		  .append("  R  : return a loan\n")
		  .append("  LL : list loans\n")
		  .append("\n")
		  .append("  P  : pay fine\n")
		  .append("\n")
		  .append("  T  : increment date\n")
		  .append("  Q  : quit\n")
		  .append("\n")
		  .append("Choice : ");
		  
		return sb.toString();
	}


	public static void main(String[] args) {		
		try {			
			in = new Scanner(System.in);
			library = Library.getInstance();
			calendar = Calendar.getInstance();
			simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
			for (Member m : library.toListMembers()) {
				output(m);
			}
			output(" ");
			for (Book b : library.toListBooks()) {
				output(b);
			}
						
			menu = getMenu();
			
			boolean e = false;
			
			while (!e) {
				
				output("\n" + simpleDateFormat.format(calendar.getDate()));
				String c = input(menu);
				
				switch (c.toUpperCase()) {
				
                                    case "M": 
                                            addMember();
                                            break;

                                    case "LM": 
                                            listMembers();
                                            break;

                                    case "B": 
                                            addBook();
                                            break;

                                    case "LB": 
                                            listBooks();
                                            break;

                                    case "FB": 
                                            fixBooks();
                                            break;

                                    case "L": 
                                            borrowBooks();
                                            break;

                                    case "R": 
                                            returnBook();
                                            break;

                                    case "LL": 
                                            listCurrentLoans();
                                            break;

                                    case "P": 
                                            payFines();
                                            break;

                                    case "T": 
                                            incrementDate();
                                            break;

                                    case "Q": 
                                            e = true;
                                            break;

                                    default: 
                                            output("\nInvalid option\n");
                                            break;
				}
				
				Library.toSave();
			}			
		} catch (RuntimeException e) {
			output(e);
		}		
		output("\nEnded\n");
	}	

	
	private static void payFines() {       //Fix method PAY_FINES --> payFines  --Ketulkumar
		new PayFineUI(new PayFineControl()).run();   //Fix method name pAY_fINE_cONTROL --> PayFineControl, RuN --> run  --Ketulkumar
	}


	private static void listCurrentLoans() {      //Fix method LIST_CURRENT_LOANS --> listCurrentLoans  --Ketulkumar
		output("");
		for (Loan loan : library.toListCurrentLoan()) {
			output(loan + "\n");
		}		
	}



	private static void listBooks() {      //Fix method LIST_BOOKS --> listBooks  --Ketulkumar
		output("");
		for (Book book : library.toListBooks()) {
			output(book + "\n");
		}		
	}



	private static void listMembers() {        //Fix method LIST_MEMBERS --> listMembers  --Ketulkumar
		output("");
		for (Member member : library.toListMembers()) {
			output(member + "\n");
		}		
	}



	private static void borrowBooks() {         //Fix method BORROW_BOOK --> borrowBooks  --Ketulkumar
		new BorrowBookUi(new BorrowBookControl()).run();  //Fix method name bORROW_bOOK_cONTROL --> FixBookControl, RuN --> run  --Ketulkumar			
	}


	private static void returnBook() {         //Fix method RETURN_BOOK --> returnBook  --Ketulkumar
		new ReturnBookUI(new ReturnBookControl()).run();		//Fix method RuN --> run  --Ketulkumar
	}


	private static void fixBooks() {           //Fix method FIX_BOOKS --> fixBooks  --Ketulkumar
		new FixBookUI(new FixBookControl()).run();	//Fix method name fIX_bOOK_cONTROL --> FixBookControl, RuN --> run  --Ketulkumar	
	}


	private static void incrementDate() {      //Fix method INCREMENT_DATE --> incrementDate  --Ketulkumar
		try {
			int days = Integer.parseInt(input("Enter number of days: "));     //Remove unnecessary unboxing
			calendar.incrementDate(days);
			library.toCheckCurrentLoan();
			output(simpleDateFormat.format(calendar.getDate()));
			
		} catch (NumberFormatException e) {
			 output("\nInvalid number of days\n");
		}
	}


	private static void addBook() {        //Fix method ADD_BOOK --> addBook  --Ketulkumar
		
		String author = input("Enter author: ");                    //Fix variable AuThOr --> author  --Ketulkumar
		String title  = input("Enter title: ");                     //Fix variable TiTlE --> title  --Ketulkumar
		String callNumber = input("Enter call number: ");          //Fix variable CaLl_NuMbEr --> callNumber  --Ketulkumar
		Book BoOk = library.toAddBook(author, title, callNumber);
		output("\n" + BoOk + "\n");
		
	}

	
	private static void addMember() {      //Fix method ADD_MEMBER --> addMember  --Ketulkumar
		try {
			String lastName = input("Enter last name: ");      //Fix variable LaSt_NaMe --> lastName  --Ketulkumar
			String firstName  = input("Enter first name: ");   //Fix variable FiRsT_NaMe --> firstName  --Ketulkumar
			String emailAddress = input("Enter email address: ");      //Fix variable EmAiL_AdDrEsS --> emailAddress  --Ketulkumar
			int phoneNumber = Integer.parseInt(input("Enter phone number: "));   //Fix variable PhOnE_NuMbEr --> phoneNumber  --Ketulkumar and Remove unnecessary unboxing
			Member member = library.toAddMembers(lastName, firstName, emailAddress, phoneNumber);   //Fix object MeMbEr --> member  --Ketulkumar
			output("\n" + member + "\n");
			
		} catch (NumberFormatException e) {
			 output("\nInvalid phone number\n");
		}
		
	}


	private static String input(String prompt) {
		System.out.print(prompt);
		return in.nextLine();
	}
	
	
	
	private static void output(Object object) {
		System.out.println(object);
	}

	
}
