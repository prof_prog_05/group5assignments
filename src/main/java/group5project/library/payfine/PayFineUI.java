package library.payfine;
import java.util.Scanner;


public class PayFineUI {//Fixed class name --Athulya

	public static enum UiState {INITIALISED, READY, PAYING, COMPLETED, CANCELLED};// Fixed enum name UiState and corrected bracket spacing-- Athulya
	
        private PayFineControl control; // Fixed control object name and PayFineControl class name--Athulya
	private Scanner input;
	private UiState state;  //Fixed state and enum name UiState---Athulya

	public PayFineUI(PayFineControl control) {// Fixed PayFineControl class name--- AThulya
		this.control = control;        // Fixed control --Athulya
		input = new Scanner(System.in);
		state = UiState.INITIALISED; //Fixed state varible --Athulya
		control.setUi(this); //Fixed method name setUi --Athulya
	}
	
	
	public void setState(UiState state) {//Fixed setState method name --Athulya
		this.state = state;
	}


	public void run() { //Fixed method name run --Athulya
		output("Pay Fine Use Case UI\n");
		
		while (true) {
			
			switch (state) {
			
			case READY:
				String memStr = input("Swipe member card (press <enter> to cancel): "); //Fixed memStr variable name--Athulya
				if (memStr.length() == 0) {
					control.cancel();  //Fixed control and method name to cancel--Athulya
					break;
				}
				try {
					int memberId = Integer.parseInt(memStr); //Fixed variable name memberId and unneccessary unboxing to integer.parseInt --Athulya
					control.swipeCard(memberId); //Fixed method name swipeCard --Athulya
				}
				catch (NumberFormatException e) {
					output("Invalid memberId");
				}
				break;
				
			case PAYING:
				double amount = 0; //Fixed variable name amount --Athulya
				String amtStr = input("Enter amount (<Enter> cancels) : ");
				if (amtStr.length() == 0) { //Fixed variable name amtStr --Athulya
					control.cancel(); //Fixed method name cancel --Athulya
					break;
				}
				try {
					amount = Double.valueOf(amtStr).doubleValue();
				}
				catch (NumberFormatException e) {}
				if (amount <= 0) {
					output("Amount must be positive");
					break;
				}
				control.payFine(amount); //Fixed method name payFine
				break;
								
			case CANCELLED:// Fixed to CANCELLED--AThulya
				output("Pay Fine process cancelled");
				return;
			
			case COMPLETED:
				output("Pay Fine process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + state);			
			
			}		
		}		
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}	
			

	public void display(Object object) {// Fixed method name display--- Athulya
		output(object);
	}


}
