package library.payfine;
import library.entities.Library;
import library.entities.Member;

public class PayFineControl { //Fixed class name PayFineControl  ---Athulya
	
	private PayFineUI ui; // Fixed  object name to ui --Athulya
	private enum controlState {INITIALISED, READY, PAYING, COMPLETED, CANCELLED};//Fixed brackets and enum name to controlState--Athulya
	private controlState state;//Fixed variable name to state--Athulya
	
	private Library library;//Fixed the object name to library--Athulya
	private Member member;//Fixed object name to member--Athulya


	public PayFineControl() {//Fixed constructor name to PayFineControl--Athulya
		this.library = Library.getInstance();//fixed the method name to getInstance//Athulya
		state = controlState.INITIALISED;
	}
	
	
	public void setUi(PayFineUI ui) {//Fixed class name to PayFineUI and method name to setUi--Athulya
		if (!state.equals(controlState.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.ui = ui;
		ui.setState(PayFineUI.UiState.READY);//Fixed method name setState and enum name to UiState--Athulya
		state = controlState.READY;		
	}


	public void swipeCard(int memberId) {//Fixed method name swipeCard--Athulya
		if (!state.equals(controlState.READY)) {//Bracket correction--Athulya
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
		}	
		member = library.toGetMember(memberId);//Fixed variable name to memberId and method name to toGetMember--Athulya
		
		if (member == null) {
			ui.display("Invalid Member Id");//Method name fixed to display --Athulya
			return;
		}
		ui.display(member.toString());
		ui.setState(PayFineUI.UiState.PAYING);
		state = controlState.PAYING;
	}
	
	
	public void cancel() {//method name changed to cancel--Athulya
		ui.setState(PayFineUI.UiState.CANCELLED);
		state = controlState.CANCELLED;
	}


	public double payFine(double amount) {//Fixed method name to payFine and variable to amount--Athulya
		if (!state.equals(controlState.PAYING)){ //Bracket correction--Athulya
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
		}	
		double change = member.toPayFine(amount);//Fixed variable name to change--Athulya
		if (change > 0) {// Bracket correction --Athulya
			ui.display(String.format("Change: $%.2f", change));
		}
		ui.display(member.toString());
		ui.setState(PayFineUI.UiState.COMPLETED);
		state = controlState.COMPLETED;
		return change;
	}
	


}
