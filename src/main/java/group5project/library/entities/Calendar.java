package library.entities;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Calendar {
	
	private static Calendar self; /*Fixed static variable name SElF to self--Athulya*/
	private static java.util.Calendar calendar; /*Fixed static variable name CALENDAR to calendar--Athulya*/
	
	
	private Calendar() {
		calendar = java.util.Calendar.getInstance();
	}
	
	public static Calendar getInstance() { /*Fixed the method name gEtInStAnCe to getInstance --Athulya*/
		if (self == null) { /**Fixed static variable sElF to self-- Athulya*/
			self = new Calendar();
		}
		return self;
	}
	
	public void incrementDate(int days) {
		calendar.add(java.util.Calendar.DATE, days);		
	}
	
	public synchronized void setDate(Date date) { /*Fixed SeT_DaTe method name to setDate and DaTe to date --Athulya*/
		try {
			calendar.setTime(date);
	                calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);  
	                calendar.set(java.util.Calendar.MINUTE, 0);  
	                calendar.set(java.util.Calendar.SECOND, 0);  //Fixed spaces---Athulya
	                calendar.set(java.util.Calendar.MILLISECOND, 0);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}	
	}
	public synchronized Date getDate() { /*Fixed gEt_DaTe method name to getDate--Athulya*/
		try {
	                calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);  
	                calendar.set(java.util.Calendar.MINUTE, 0);  
	                calendar.set(java.util.Calendar.SECOND, 0);  
	                calendar.set(java.util.Calendar.MILLISECOND, 0);
			return calendar.getTime();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}	
	}

	public synchronized Date getDueDate(int loanPeriod) {/*fixed method name gEt_DuE_DaTe to getDueDate --Athulya*/
		Date now = getDate(); /*Fixed variable name nOw to now --Athulya*/
		calendar.add(java.util.Calendar.DATE, loanPeriod);
		Date dueDate = calendar.getTime();/*Fixed variable name dUeDaTe to dueDate --Athulya*/
		calendar.setTime(now);
		return dueDate;
	}
	
	public synchronized long toGetDaysDifference(Date targetDate) { /*Fixed method name GeT_DaYs_DiFfErEnCe to toGetDaysDifference--Athulya*/
		
		long diffMillis = getDate().getTime() - targetDate.getTime(); /*Fixed variable name Diff_Millis to diffMillis--Athulya*/
	        long diffDays = TimeUnit.DAYS.convert(diffMillis, TimeUnit.MILLISECONDS);/*Fixed variable name Diff_Days to diffDays--Athulya*/
	        return diffDays;
	}

}
