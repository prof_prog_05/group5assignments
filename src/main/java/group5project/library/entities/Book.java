package library.entities;
import java.io.Serializable;


@SuppressWarnings("serial")
public class Book implements Serializable {
	
	private String title;   //format correction of tItLe --> title
	private String author;  //format correction of AuThOr --> author
	private String callNo;  //format correction of CALLNO --> callNo
	private int id;         //format correction of iD --> id
	
	private enum State {AVAILABLE, ON_LOAN, DAMAGED, RESERVED};    //format correction of sTaTe enum data type --> State
	private State state;    //formar correction of StAtE --> state
	
	
	public Book(String author, String title, String callNo, int id) {
		this.author = author;
		this.title = title;
		this.callNo = callNo;
		this.id = id;
		this.state = State.AVAILABLE;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Book: ").append(id).append("\n")
		  .append("  Title:  ").append(title).append("\n")
		  .append("  Author: ").append(author).append("\n")
		  .append("  CallNo: ").append(callNo).append("\n")
		  .append("  State:  ").append(state);
		
		return sb.toString();
	}
        
	public Integer getId() {        //format correction of gEtId method --> getId
		return id;
	}

	public String getTitle() {      //format correction of gEtTiTlE method --> getTitle
		return title;
	}


	
	public boolean isAvailable() {      //format correction of iS_AvAiLaBlE method --> isAvailable
		return state == State.AVAILABLE;
	}

	
	public boolean isOnLoan() {         //format correction of iS_On_LoAn method --> isOnLoan
		return state == State.ON_LOAN;
	}

	
	public boolean isDamaged() {        //format correction of iS_DaMaGeD method --> isDamaged
		return state == State.DAMAGED;
	}

	
	public void borrow() {              //format correction of BoRrOw method --> borrow
		if (state.equals(State.AVAILABLE)) 
			state = State.ON_LOAN;
		
		else 
			throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));
		
		
	}


	public void returnBook(boolean damaged) {       //format and name correction of ReTuRn method --> returnBook, fix DaMaGeD variable --> damaged
		if (state.equals(State.ON_LOAN)) 
			if (damaged) 
				state = State.DAMAGED;
			
			else 
				state = State.AVAILABLE;
			
		
		else 
			throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
				
	}

	
	public void toRepair() {           //format correction of RePaIr method --> toRepair
		if (state.equals(State.DAMAGED)) 
			state = State.AVAILABLE;
		
		else 
			throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
		
	}


}
