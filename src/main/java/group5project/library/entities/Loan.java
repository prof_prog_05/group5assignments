package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {

    void remove(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	
	public static enum LoanState {CURRENT, OVER_DUE, DISCHARGED};     //format correction of lOaN_sTaTe enum --> LoanState 
	
	private int loanId;        //format correction of LoAn_Id --> loanId
	private Book book;          //format correction of BoOk --> book
	private Member member;      //format correction of MeMbEr --> member
	private Date date;          //format correction of DaTe --> date
	private LoanState state;    //format correction of StAtE --> state

	
	public Loan(int loanId, Book book, Member member, Date dueDate) {      //fix bOoK --> book, mEmBeR --> member, DuE_dAtE --> dueDate
		this.loanId = loanId;
		this.book = book;
		this.member = member;
		this.date = dueDate;
		this.state = LoanState.CURRENT;
	}

	
	public void checkOverDue() {          //format correction of method cHeCk_OvEr_DuE --> checkOverDue
		if (state == LoanState.CURRENT &&
			Calendar.getInstance().getDate().after(date)) //format correction of method gEtInStAnCe --> getInstance, gEt_DaTe-->getDate
			this.state = LoanState.OVER_DUE;			
		
	}

	
	public boolean isOverDue() {          //format correction of method Is_OvEr_DuE --> isOverDue
		return state == LoanState.OVER_DUE;
	}

	
	public Integer getId() {           //format correction of method GeT_Id --> getId
		return loanId;
	}


	public Date getDueDate() {           //format correction of method GeT_DuE_DaTe --> getDueDate
		return date;
	}
	
	
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		StringBuilder sb = new StringBuilder();
		sb.append("Loan:  ").append(loanId).append("\n")
		  .append("  Borrower ").append(member.getId()).append(" : ")  //Fix method GeT_ID --> getId --Ketul
		  .append(member.getLastName()).append(", ").append(member.getFirstName()).append("\n")     //Fix method GeT_LaSt_NaMe --> getLastName and GeT_FiRsT_NaMe --> getFirstName --Ketul
		  .append("  Book ").append(book.getId()).append(" : " )        //fix gEtId method --> getId
		  .append(book.getTitle()).append("\n")                         //fix gEtTiTlE method --> getTitle
		  .append("  DueDate: ").append(sdf.format(date)).append("\n")
		  .append("  State: ").append(state);		
		return sb.toString();
	}


	public Member getMember() {         //format correction of method GeT_MeMbEr --> getMember
		return member;
	}


	public Book getBook() {            //format correction of method GeT_BoOk --> getBook
		return book;
	}


	public void discharge() {
		state = LoanState.DISCHARGED;        //format correction of method DiScHaRgE --> discharge
	}

}
