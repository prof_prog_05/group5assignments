package library.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Member implements Serializable {

	private String lastName; /*Removed _ and made variable to camelBack  Athulya*/
	private String firstName; /*Removed _ and made variable to camelBack  Athulya*/ 
	private String emailAddress; /*Removed _ and changed varible to camelBack - Athulya*/
	private int phoneNumber; /*Removed _ and changed variable to camelBack -Athulya*/
	private int memberId; /*Removed _ and changed variable to camelBack -Athulya*/
	private double finesOwing; /*Removed _ and changed variable to camelBack- Athulya*/
	
	private Map<Integer, Loan> currentLoans;/*Removed _ and changed to camelBack-Athulya*/

	
	public Member(String lastName, String firstName, String emailAddress, int phoneNumber, int memberId) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		this.memberId = memberId;
		
		this.currentLoans = new HashMap<>();
	}

	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Member:  ").append(memberId).append("\n")
		  .append("  Name:  ").append(lastName).append(", ").append(firstName).append("\n")
		  .append("  Email: ").append(emailAddress).append("\n")
		  .append("  Phone: ").append(phoneNumber)
		  .append("\n")
		  .append(String.format("  Fines Owed :  $%.2f", finesOwing))
		  .append("\n");
		
		for (Loan loan : currentLoans.values()) {
			sb.append(loan).append("\n");
		}	/*corrected object name loan -Athulya*/	  
		return sb.toString();
	}

	
	public int getId() {/*changed function name -Athulya*/
		return memberId;
	}

	
	public List<Loan> getLoans() { /*Changed function name --Athulya*/
		return new ArrayList<Loan>(currentLoans.values());
	}

	
	public int getNumberOfCurrentLoans() {/*Changed function name--Athulya*/
		return currentLoans.size();
	}

	
	public double finesOwed() { /*Changed function name-- Athulya*/
		return finesOwing;
	}

	
	public void toTakeOutLoan(Loan loan) { /*Changed function name--Athulya*/
		if (!currentLoans.containsKey(loan.getId())) { // Added brackets ---Athulya
			currentLoans.put(loan.getId(), loan);
		}
		else {//Added brackets-- Athulya
			throw new RuntimeException("Duplicate loan added to member");
		}		
	}

	
	public String getLastName() { /*Changed method name --Athulya*/
		return lastName;
	}

	
	public String getFirstName() { /*Changed method name--Athulya*/
		return firstName;
	}


	public void toAddFine(double fine) {/*Changed method name --Athulya*/
		finesOwing += fine;
	}
	
	public double toPayFine(double amount) { /*changed method name and amount variable name*/
		if (amount < 0) {
			throw new RuntimeException("Member.payFine: amount must be positive");
		
                }/*Added brackets--Athulya*/
		double change = 0;
		if (amount > finesOwing) {
			change = amount - finesOwing;
			finesOwing = 0;
		}
		else { 
			finesOwing -= amount;
		}
		return change;
	}


	public void toDischargeLoan(Loan loan) {/*changed method name and object name--Athulya*/
		if (currentLoans.containsKey(loan.getId())) {
			currentLoans.remove(loan.getId());
		}/*Added brackets*/
		else  {
			throw new RuntimeException("No such loan held by member");
                }/*Added brackets*/
				
	}

}
