package library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {
	
	private static final String LIBRARY_FILE = "library.obj";           //Updated constant variable
	private static final int LOAN_LIMIT = 2;                            //Updated constant LOAN_LIMIT variable
	private static final int LOAN_PERIOD = 2;                           //Updated constant variable
	private static final double FINE_PER_DAY = 1.0;                     //Updated constant FINE_PER_DAY variable
	private static final double MAX_FINES_OWED = 1.0;                   //Updated constant variable
	private static final double DAMAGE_FEE = 2.0;                       //Updated constant variable
	
	private static Library self;        //Fixed self variablex
	private int bookId;                 //Fixed bookId variable
	private int memberId;               //Fixed memberId variable
	private int loanId;                 //Fixed loanId variable
	private Date loanDate;              //Fixed loanDate variable
	
	private Map<Integer, Book> catalog;     //Fixed catalog
	private Map<Integer, Member> members;   //Fixed members
	private Map<Integer, Loan> loans;       //Fixed loans
	private Map<Integer, Loan> currentLoan; //Fixed currentLoan
	private Map<Integer, Book> damagedBooks; //Fixed damagedBooks
	

	private Library() {
                catalog = new HashMap<>();
		members = new HashMap<>();
		loans = new HashMap<>();
		currentLoan = new HashMap<>();
		damagedBooks = new HashMap<>();
		bookId = 1;
		memberId = 1;		
		loanId = 1;		
	}

        //Fixed all the brackets foe nested loops
	
	public static synchronized Library getInstance() {                  //Fixed getInstance Function
		if (self == null) {
			Path path = Paths.get(LIBRARY_FILE);                //Fixed object for path class
			if (Files.exists(path)) {	
				try (ObjectInputStream libraryFile = new ObjectInputStream(new FileInputStream(LIBRARY_FILE));) {   //Fixed libraryFile local variable
			    
					self = (Library) libraryFile.readObject();
					Calendar.getInstance().setDate(self.loanDate);         //Fixed method for setting date                        //Fixed method for getInstance from Clander class
					libraryFile.close();
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
                        else{ 
                            self = new Library();
                        }    
		}
                        
		return self;
	}

	
	public static synchronized void toSave() {                                  //Fixed the method for saving the dates
		if (self != null) {
			self.loanDate = Calendar.getInstance().getDate();           //Fixed method for getDate
			try (ObjectOutputStream libraryFile = new ObjectOutputStream(new FileOutputStream(LIBRARY_FILE));) {            //Fixed local variable libraryFile
				libraryFile.writeObject(self);
				libraryFile.flush();
				libraryFile.close();	
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public int getBookId() {                //Fixed bookid method
		return bookId;
	}
	
	
	public int getMemberId() {              //Fixed method for getting memberId
		return memberId;
	}
	
	
	private int getNextBookId() {           //Fixed method for getting nextbookid
		return bookId++;
	}

	
	private int getNextMemberId() {            //Fixed method for getting nextmemberId
		return memberId++;
	}

	
	private int getNextLoanId() {               //Fixed method for getting nextLoanId
		return loanId++;
	}

	
	public List<Member> toListMembers() {                       //Fixed method for toListMembers
		return new ArrayList<Member>(members.values()); 
	}


	public List<Book> toListBooks() {                           //Fixed method for toListBooks
		return new ArrayList<Book>(catalog.values()); 
	}


	public List<Loan> toListCurrentLoan() {                     //Fixed method for lisiting currentLoans
		return new ArrayList<Loan>(currentLoan.values());
	}


	public Member toAddMembers(String lastName, String firstName, String email, int phoneNo) {	//Fixed method to add members	
		Member member = new Member(lastName, firstName, email, phoneNo, getNextMemberId());
		members.put(member.getId(), member);		
		return member;
	}

	
	public Book toAddBook(String a, String t, String c) {                           //Fixed method for adding book
		Book b = new Book(a, t, c, getNextBookId());
		catalog.put(b.getId(), b);                                            //Fixed method for getting id
		return b;
	}

	
	public Member toGetMember(int memberId) {                                       //Fixed method for getting members
		if (members.containsKey(memberId)) 
			return members.get(memberId);
		return null;
	}

	
	public Book toGetBook(int bookId) {                                             //Fixed method for getting books
		if (catalog.containsKey(bookId)){ 
			return catalog.get(bookId);
                }        
		return null;
	}

	
	public int toGetLoanLimit() {                                                   //Fixed method for getting loan limit
		return LOAN_LIMIT;
	}

	
	public boolean canMemberBorrow(Member member) {                                 //Fixed method for borrowing members
		if (member.getNumberOfCurrentLoans() == LOAN_LIMIT ){                     //Fixed method for getting number of current loans
			return false;
                }		
		if (member.finesOwed() >= MAX_FINES_OWED){                               //Fixed method for fines owed
			return false;
                }		
		for (Loan loan : member.getLoans()){                                     //Fixed method for getting loans
			if (loan.isOverDue()){                                           //Fixed method for loan overdue
				return false;
                        }
                }
            return true;
	}

	
	public int getNumberOfLoansRemainingForMember(Member member) {                  //Fixed method of getting the remaining loan for members
		return LOAN_LIMIT - member.getNumberOfCurrentLoans();                    //Fixed member object
	}

	
	public Loan issueLoan(Book book, Member member) {                               //Fixed method for issueLoans    
		Date dueDate = Calendar.getInstance().getDueDate(LOAN_PERIOD);          //Fixed method for getting due dates
		Loan loan = new Loan(getNextLoanId(), book, member, dueDate);
		member.toTakeOutLoan(loan);                                             //Fixed method for taking out loans
		book.borrow();//Fixed method for borrowimg the book
		loans.put(loan.getId(), loan);
		currentLoan.put(book.getId(), loan);
		return loan;
	}
	
	
	public Loan toGetLoanByBookId(int bookId) {                                     //Fixed method for getting loan by bookid
		if (currentLoan.containsKey(bookId)){                                   //Fixed brackets for nested loop
			return currentLoan.get(bookId);
                }	
                return null;
	}

	
	public double toCalculateOverDueFine(Loan loan) {                               //fixed method for calculating over due fines
		if (loan.isOverDue()) {                                                 //Fixed object for Loan method
			long daysOverDue = Calendar.getInstance().toGetDaysDifference(loan.getDueDate());     //Fixed methods 
			double fine = daysOverDue * FINE_PER_DAY;                       //Fixed variable for daysOverDue
			return fine;                                                    //Fixed variable fine
		}
		return 0.0;		
	}
        
        
        //Fixed method for discharging loan 
            
	public void toDischargeLoan(Loan currentLoan, boolean isDamaged) {            //Fixed objects for Loan and parameter for the method
		Member member = currentLoan.getMember();                            //Fixed method for getting member 
		Book book  = currentLoan.getBook();                                  //Fixed object for the method and fixed method for getting books 
		
		double overDueFine = toCalculateOverDueFine(currentLoan);             //Fixed variable for overduefine
		member.toAddFine(overDueFine);                                        //Fixed object for member method
		
		member.toDischargeLoan(currentLoan);                                   //Fixed method for discharging loan 
		book.returnBook(isDamaged);                                            //Fixed method for returing 
		if (isDamaged) {
			member.toAddFine(DAMAGE_FEE);                                //Fixed method for adding fine
			damagedBooks.put(book.getId(), book);
		}
		currentLoan.discharge();                                               //Fixed method for discharging
		currentLoan.remove(book.getId());
	}


	public void toCheckCurrentLoan() {                                           //Fixed method for checking current loan
		for (Loan loan : currentLoan.values()){                               //Fixed obeject for Loan class
			loan.checkOverDue();//Fixed method for checking over Due   
                }		
	}


	public void toRepairBook(Book currentBook) {                                    //Fixed method for repair book as well as fixed object of book class     
		if (damagedBooks.containsKey(currentBook.getId())) {
			currentBook.toRepair();                                         //Fixed method for repairing the books 
			damagedBooks.remove(currentBook.getId());
		}
                else{                                                                   //Fixed brackets for loop         
			throw new RuntimeException("Library: repairBook: book is not damaged");
                }
		
	}
	
	
}
