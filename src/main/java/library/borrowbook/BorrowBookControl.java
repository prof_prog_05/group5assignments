package library.borrowbook;
import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {                                //Fixed the class name --> BorrowBookControl
	
	private BorrowBookUi ui;                                //Fixed the object name -->ui
	
	private Library library;                                //Fixed the object name for Library class --> library
	private Member member;                                  //Fixed the object name for Member class --> member
	private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private ControlState state;                             //Fixed the enum name --> ControlState and object name
	
	private List<Book> pendingList;                         //Fixed the variable name --> pending List
	private List<Loan> completedList;                       //Fixed the variable name --> completedList
	private Book book;                                      //Fixed the object name --> book
	
	
	public BorrowBookControl() {
		this.library = Library.getInstance();           //Fixed the method name --> getInstance
		state = ControlState.INITIALISED;
	}
	

	public void setUi(BorrowBookUi ui) {                    //Fixed the method name --> setUi
		if (!state.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.ui = ui;                                   //Fixed the object name --> ui
		ui.setState(BorrowBookUi.UiState.READY);        //Fixed the class name --> BorrowBookUi
		state = ControlState.READY;                     //Fixed the method name --> setState and enum name --> UiState
	}

		
	public void swiped(int memberId) {                     //Fixed the class name--> swiped and variable name --> memberId     
		if (!state.equals(ControlState.READY)) 
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		member = library.toGetMember(memberId);            //Fixed the method name --> toGetMember
		if (member == null) {
			ui.display("Invalid memberId");             //Fixed the method name --> display
			return;
		}
		if (library.canMemberBorrow(member)) {             //Fixed the method name --> canMemberName
			pendingList = new ArrayList<>();
			ui.setState(BorrowBookUi.UiState.SCANNING);
			state = ControlState.SCANNING; 
		}
		else {
			ui.display("Member cannot borrow at this time");
			ui.setState(BorrowBookUi.UiState.RESTRICTED); 
		}
	}
	
	
	public void scanned(int bookId) {                       //Fixed the method name --> scanned 
		book = null;
		if (!state.equals(ControlState.SCANNING)) 
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
			
		book = library.toGetBook(bookId);                //Fixed the object name --> bookId
		if (book == null) {                              //Fixed the method name --> toGetBook
			ui.display("Invalid bookId");
			return;
		}
		if (!book.isAvailable()) {                      //Fixed the method name --> isAvailable
			ui.display("Book cannot be borrowed");
			return;
		}
		pendingList.add(book);
		for (Book B : pendingList) 
			ui.display(B.toString());
		
		if (library.getNumberOfLoansRemainingForMember(member) - pendingList.size() == 0) {         //Fixed the method name --> getNumberOfLoansRemainingForMember
			ui.display("Loan limit reached");
			complete();                                 //Fixed the method name --> complete
		}
	}
	
	
	public void complete() {
		if (pendingList.size() == 0) 
			cancel();                                   //Fixed the method name --> cancel
		
		else {
			ui.display("\nFinal Borrowing List");
			for (Book book : pendingList) 
				ui.display(book.toString());
			
			completedList = new ArrayList<Loan>();
			ui.setState(BorrowBookUi.UiState.FINALISING);
			state = ControlState.FINALISING;
		}
	}


	public void commitLoans() {                                 //Fixed the method name --> commitLoans
		if (!state.equals(ControlState.FINALISING)) 
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
			
		for (Book B : pendingList) {
			Loan loan = library.issueLoan(B, member);      //Fixed the object name--> loan and the method name --> issueLoan
			completedList.add(loan);			
		}
		ui.display("Completed Loan Slip");
		for (Loan LOAN : completedList) 
			ui.display(LOAN.toString());
		
		ui.setState(BorrowBookUi.UiState.COMPLETED);
		state = ControlState.COMPLETED;
	}

	
	public void cancel() {
		ui.setState(BorrowBookUi.UiState.CANCELLED);
		state = ControlState.CANCELLED;
	}
	
	
}
