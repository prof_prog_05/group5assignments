package library.borrowbook;

import java.util.Scanner;


public class BorrowBookUi {                      //Fixed the class name --> BookBookUi
                                                //Fixed class name UiState --> UiState
                                               //Updated the parameter passed to the class
	public static enum UiState{ INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };

	private BorrowBookControl control;                                  //Fixed the class name 
	private Scanner input;                                              //Fixed the object of the class scanner
	private UiState state;                                              //Fixed the object of the class UiState

	
	public BorrowBookUi(BorrowBookControl control) {
		this.control = control;                                 //Fixed the object of class borrowbookcontrol
		input = new Scanner(System.in);
		state = UiState.INITIALISED;                            //Fixed Class name --> UiState
		control.setUi(this);                                    //Fixed method name --> setUi
	}

	
	private String Input(String prompt) {                          //Fixed method name --> Input   
		System.out.print(prompt);                              //Fixed variable name --> prompt
		return input.nextLine();
	}	
		
		
	private void output(Object object) {                           //Fixed method name --> output
		System.out.println(object);                            //Fixed the object of Object class --> object
	}
	
			
	public void setState(UiState uiState) {                          //Fixed the method name --> setState
		this.state = uiState;                                    //Fixed the object name --> uiState
	}

	
	public void run() {                                             //Fixed the method name --> run
		output("Borrow Book Use Case UI\n");
		
		while (true) {
			
			switch (state) {			
			
			case CANCELLED:
				output("Borrowing Cancelled");
				return;

				
			case READY:
				String memberString = Input("Swipe member card (press <enter> to cancel): ");
				if (memberString.length() == 0) {                               //Fixed the variable name MEM_STR --> memberString
					control.cancel();                                       //Fixed the method name --> cancel
					break;
				}
				try {
					int memberId = Integer.parseInt(memberString);          //Fixed unnecessary unboxing
					control.swiped(memberId);                               //Fixed the variable name --> memberId
				}                                                               //Fixed the method name --> swiped
				catch (NumberFormatException e) {
					output("Invalid Member Id");
				}
				break;

				
			case RESTRICTED:
				Input("Press <any key> to cancel");
				control.cancel();
				break;
			
				
			case SCANNING:                                          //Fixed the variable name -->bookStringInput
				String bookStringInput = Input("Scan Book (<enter> completes): ");
				if (bookStringInput.length() == 0) {
					control.complete();                     //Fixed method name --> complete
					break;
				}
				try {
					int bookId = Integer.parseInt(bookStringInput);     //Fixed unnecessary unboxing
					control.scanned(bookId);                            //Fixed variable name --> bookId and method name --> scanned
					
				} catch (NumberFormatException e) {
					output("Invalid Book Id");
				} 
				break;
					
				
			case FINALISING:
				String answer = Input("Commit loans? (Y/N): ");             //Fixed variable name --> answer
				if (answer.toUpperCase().equals("N")) {
					control.cancel();
					
				} else {
					control.commitLoans();                          //Updated the method name --> commitLoans
					Input("Press <any key> to complete ");
				}
				break;
				
				
			case COMPLETED:
				output("Borrowing Completed");
				return;
	
				
			default:
				output("Unhandled state");
				throw new RuntimeException("BorrowBookUI : unhandled state :" + state);			
			}
		}		
	}


	public void display(Object object) {                                //Fixed the method name --> display
		output(object);		
	}


}
